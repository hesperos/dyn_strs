#include "foo.h"
#include "es.h"

#include <stdio.h>


void hello(void) {
	_ES_DECL(hello_msg, "This is a hello message in file foo.c");
	printf("%s(): %s\n", __FUNCTION__, _ES_DEC(hello_msg));
}
