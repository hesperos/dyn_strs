TARGET=encrypted_strings_test
SECRET=secret.txt
CSRC= \
	test.c \
	foo.c

ES_SRC=$(CSRC:.c=_es.c)
ES_OBJ=$(ES_SRC:.c=.o)

OBJ=$(ES_OBJ)
# OBJ=$(CSRC:.c=.o)

CC=gcc
CFLAGS=-I. -Wall -D_ES_ENABLED=0
LDFLAGS=-lmangle -Llibmangle/ 
LIBMANGLE=libmangle.a

all: $(TARGET)
.PHONY: clean $(LIBMANGLE) $(GENDIR)
.PRECIOUS: $(ES_SRC)

%_es.c: %.c
	@echo -e "\tGEN" $@ $<
	@./es.pl -v -k $(SECRET) $<


%_es.o: %_es.c
	@echo -e "\tCC" $@ $<
	@$(CC) -c -o $@ $< $(CFLAGS)

$(LIBMANGLE):
	$(MAKE) -C libmangle/

$(TARGET): $(LIBMANGLE) $(OBJ)
	@echo -e "\tLD" $(TARGET)
	@$(CC) -o $(TARGET) $(OBJ) $(LDFLAGS)

clean:
	$(MAKE) -C libmangle/ clean
	rm -rf _Inline/ *.s *.o $(TARGET)	
	
