#!/usr/bin/env perl

use strict;
use warnings;
$|++;

use Getopt::Long;
use Pod::Usage;
use Cwd;
use File::Basename;
use Inline 'C' => 'Config' => 'LIBS' => '-L' . getcwd . '/libmangle/ -lmangle';
use Inline 'C';


my %config = (
	'suffix' => 'es',
	'key_file' => undef,
	'key' => [],
	'help' => 0,
	'verbose' => 0,
);


GetOptions(
	"k|key-file=s" => \$config{'key_file'},
	"v|verbose+" => \$config{'verbose'},
	"s|suffix=s" => \$config{'suffix'},
	"h|help" => \$config{'help'},
) or pod2usage();


pod2usage(1) if $config{'help'};
die "No key file provided\n" unless $config{'key_file'};


# open the key file in binary mode
open my $key_fh, "<", $config{'key_file'} or 
	die "Unable to open key file: " . $config{'key_file'} . "\n";
binmode $key_fh;


# read first 16 bytes and treat it as 4 unsigned long numbers
read $key_fh, my $key_data, 16;


# set the key
$config{'key'} = [ (unpack "L4", $key_data) ];
close $key_fh;


# process the input files
foreach my $f (@ARGV) {
	print "processing file: " . $f . " ... \n" if $config{'verbose'};
	process_source_file($f);
}


# process a single file
sub process_source_file {
	my $fn_in = shift;
	my @fn_in_info = fileparse($fn_in, ( ".c", ".h", ".cpp" ));

	# insert the suffix into the filename
	my $fn_out = $fn_in_info[1] . 
			$fn_in_info[0] . 
			"_" . $config{'suffix'} .
			$fn_in_info[2];

	open my $fh_in, "<", $fn_in or return -1;
	open my $fh_out, ">", $fn_out or return -1;

	while (<$fh_in>) {
		my @data = m/^(\s*)_ES_DECL\(([^,\s]+),\s*\"(.*)\"/;

		if (scalar @data == 3) {
			# we've got a match
			my $var = $data[1];
			my $len = serialized_length(length($data[2]) + 1);
			my $obf = xtea_encode($data[2], $config{'key'});

			print $fh_out $data[0] . "char $var" . "[$len] = { \"$obf\" };\n";
			next;
		}

		print $fh_out $_;
	}

	close $fh_in;
	close $fh_out;
}

# call garbage collection
mangle_garbage_collect();

__END__

=head1 es.pl

es.pl - encoded strings preprocessor.

=head1 SYNOPSIS

Parses a C source files and encrypts the strings on the fly so, the resulVting
compiled executable doesn't contain any retrievable static strings in it.

es.pl <options>

 Options:
  -k | --key-file=key_file.txt
  -s | --suffix=SUFFIX
  -v | --verbose
  -h | --help

=head1 OPTIONS

=over 8

=item B<--help>

Print this help message.

=item B<--verbose>

Increase verbosity

=item B<--suffix>

Configure the suffix for the output files (default: _es)

=item B<--key>

Configure the key file. The key file must be at least 16 bytes long.

=back

=cut

__C__
#include "libmangle/mangle.h"
#include <stdint.h>
#include <string.h>


unsigned int serialized_length(unsigned int s) {
	return mangle_serialize_get_size( mangle_xtea_get_size(s) );
}


char* xtea_encode(char *str, AV* key) {
	char stmp[8192] = {0x00};
	xtea_key_t tmp = {0x00};
	SV** elem;
	int i = 0;
	int ms = sizeof(tmp)/sizeof(tmp[0]);

	strcpy(stmp, str);
	for(;i < ms && i <= av_len(key); i++) {
		elem = av_fetch(key, i, 0);
		tmp[i] = SvNV(*elem);
	}
	return mangle_xtea_encode(stmp, tmp);
}

void mangle_garbage_collect() {
	mangle_gc();
}
