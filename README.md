String encryption on the fly.

Pre-requisites:

- libmangle:

git clone https://github.com/dagon666/libmangle

- Working installation of Perl and Inline::C module:

perl -MCPAN -e shell
install Inline::C

To build simply type make
