#ifndef ES_H_MTO1JT8W
#define ES_H_MTO1JT8W


/**
 * declare obfuscated string.
 * The Perl pre-processor should:
 *  encode and serialize the string
 *  do a proper declaration with size adjusted to the xtea blocks
 * 
 */
#define _ES_DECL(__var, __string) char __var[] = { __string }

#if _ES_ENABLED == 1


#include "libmangle/mangle.h"


/**
 * this is an interface for C application
 */

#define _ES_DEC(__string) mangle_xtea_dec(__string, xtea_default_key)
#define _ES_DEC_CONST(__string) mangle_xtea_dec_const(__string, xtea_default_key)
#define _ES_GC() mangle_gc()

#else

#define _ES_DEC(__string) __string
#define _ES_DEC_CONST(__string) __string
#define _ES_GC()

#endif

#endif /* end of include guard: ES_H_MTO1JT8W */
