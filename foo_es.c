#include "foo.h"
#include "es.h"

#include <stdio.h>


void hello(void) {
	char hello_msg[68] = { "`lX>N>4idNBWmDBekYY[oT>fJJb8C^;IM:?M8?Szognb8c99Bd=3G522" };
	printf("%s(): %s\n", __FUNCTION__, _ES_DEC(hello_msg));
}
