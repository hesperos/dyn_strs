#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "es.h"
#include "foo.h"


#if _ES_ENABLED == 1
void configure_key() {
	FILE *f = fopen("secret.txt", "r");
	xtea_key_t key;

	if (!f) {
		perror("unable to open secret.txt");
		return;
	}

	fread(key, sizeof(key), 1, f);
	fclose(f);
	mangle_xtea_set_key(key);
}
#endif


int main(int argc, char *argv[]) {
	char s1[76] = { "Qe_fV5a6SgaI>g6hWP=cgQTaKhGL=NJZJpCZaCI??Cg7f59RjJIZi@i@BcO@NbHO2222" };

#if _ES_ENABLED == 1
	configure_key();
#endif

	printf("%s\n", _ES_DEC(s1));
	hello();

	_ES_GC();
	return 0;
}
